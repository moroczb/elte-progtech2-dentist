# README #
 

### Az itt található programkódról ###

* Ez a repository tartalmazza az ELTE Programtervező Informatikus szak Programozási Technológiák 2. tárgyának reprezentatív anyagát. 

**FONTOS: A forrás fejlesztés alatt áll, használata kizárlóag saját felelősségre. ** 


### Használt eszközök ###
* [IntelliJ Idea](https://www.jetbrains.com/idea/download/)
* Java 8 SDK
* Persitence (JPA)
* EclipseLink
* Adatbázis motor: Derby

### Dokumentálsi eszközök
* markdown
* [UML] (www.nomnoml.com)

### Használati esetdiagrammok ###
![Use Case](/docs/images/usecase.png)

### Folyamatábrák ###
(__feltesszük, hogy az alkalmazás már elindult__)   

* Páciens hozzáadása   

![New patient](/docs/images/newPatient.png)  

* Páciens adatainak megjelenítése  

![show patient](/docs/images/showPatient.png)  

* Páciens Törlése  

![remove patient](/docs/images/removePatient.png)  

* Kezelés hozzáadása  

![newTreatment](/docs/images/newTreatment.png)  


### Adatmodell ###