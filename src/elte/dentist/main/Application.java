package elte.dentist.main;

import elte.dentist.main.view.frame.AppFrame;

/**
 * Created by Mórocz Bálint on 2016. 08. 22..
 */
public class Application {
    public static void main(String[] args){
        new AppFrame().setVisible(true);
    }
}
