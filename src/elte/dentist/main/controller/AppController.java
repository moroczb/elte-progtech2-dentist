package elte.dentist.main.controller;

import elte.dentist.main.model.datasource.DataSource;
import elte.dentist.main.model.datasource.MockDataSource;
import elte.dentist.main.model.entities.Patient;
import elte.dentist.main.view.panel.DetailPanel;
import elte.dentist.main.view.panel.PatientPanel;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Mórocz Bálint on 2016. 08. 22..
 */
public class AppController implements PatientPanelStateController,DetailPanelStateController {

    //Datasource
    private DataSource ds = new MockDataSource();

    private DetailPanel detailView;
    private PatientPanel patientView;

    public AppController(DataSource ds, PatientPanel patientView, DetailPanel detailView) {
        this.ds = ds;
        this.detailView = detailView;
        detailView.setController(this);
        this.patientView = patientView;
        patientView.setController(this);

        initList();

    }

    public DetailPanel getDetailView() {
        return detailView;
    }

    public PatientPanel getPatientView() {
        return patientView;
    }


    @Override
    public void onChangeSelectedPatient(int selectedIndex) {
        if(selectedIndex == -1 && ds.getPatients().size() > 0){
            detailView.setPatient((Patient) ds.getPatients().get(0));
        }else{
            detailView.setPatient((Patient) ds.getPatients().get(selectedIndex));
        }

    }

    @Override
    public void onAddPatient(Patient p) {
       ds.addPatient(p);

    }

    @Override
    public void onRemovePatient(long index) {
        ds.removePatient(ds.getPatients().get((int)index).getId());
    }

    @Override
    public Class getPatientModel() {
        return ds.getPatientModel();
    }


    public void initList() {
        List<String> labels= new LinkedList<>();
        ds.getPatients().forEach(x -> labels.add(x.getName()));
        patientView.initList(labels.toArray());
    }


    @Override
    public void onChangePatient(Patient p) {
        ds.updatePatient(p);
    }

    @Override
    public Class getTreatmentModel() {
        return ds.getTreatmentModel();
    }
}
