package elte.dentist.main.controller;

import elte.dentist.main.model.entities.Patient;

/**
 * Created by Mórocz Bálint on 2016. 08. 22..
 */
public interface DetailPanelStateController {
    void onChangePatient(Patient p);
    Class getTreatmentModel();
}
