package elte.dentist.main.controller;

import elte.dentist.main.model.entities.Patient;

/**
 * Created by Mórocz Bálint on 2016. 08. 22..
 */
public interface PatientPanelStateController {
    void onChangeSelectedPatient(int selectedIndex);
    void onAddPatient(Patient p);
    void onRemovePatient(long index);
    Class getPatientModel();
}
