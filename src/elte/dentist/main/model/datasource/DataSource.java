package elte.dentist.main.model.datasource;

import elte.dentist.main.model.entities.Patient;

import java.util.List;

/**
 * Created by Mórocz Bálint on 2016. 08. 30..
 */
public interface DataSource {
    List<Patient> getPatients();
    void removePatient(long id);
    void removePatient(Patient p);
    Patient findPatient(long id);
    void updatePatient(Patient p);
    void addPatient(Patient p);
    Class getPatientModel();
    Class getTreatmentModel();
}
