package elte.dentist.main.model.datasource;

import elte.dentist.main.model.entities.Patient;
import elte.dentist.main.model.entities.jpa.JpaPatientModel;
import elte.dentist.main.model.entities.jpa.JpaTreatmentModel;

import javax.persistence.*;
import java.util.List;

/**
 * Created by shiro on 9/8/16.
 */
public class JpaDataSource implements DataSource {

    private EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("DENTIST_PU");
    private EntityManager entitymanager = emfactory.createEntityManager();


    @Override
    public List<Patient> getPatients() {
        TypedQuery<Patient> query = entitymanager.createNamedQuery("Patient.findAll",getPatientModel());
        return query.getResultList();
    }

    @Override
    public void removePatient(long id) {
        entitymanager.getTransaction().begin();

       JpaPatientModel pat= entitymanager.find(JpaPatientModel.class,id);

        entitymanager.remove(pat);
        entitymanager.getTransaction().commit();
    }

    @Override
    public void removePatient(Patient p) {
        removePatient(p.getId());
    }

    @Override
    public Patient findPatient(long id) {
        return (Patient)entitymanager.find(getPatientModel(),id);
    }

    @Override
    public void updatePatient(Patient p) {
        entitymanager.getTransaction().begin();

        JpaPatientModel pat = (JpaPatientModel) entitymanager.find(getPatientModel(),p.getId());
        System.out.println(pat);

        pat.setTreatments(p.getTreatments());
        pat.setAddress(p.getAddress());
        pat.setName(p.getName());
        pat.setDateOfBirth(p.getDateOfBirth());
        System.out.println(pat);
        entitymanager.merge(pat);
        entitymanager.getTransaction().commit();
    }

    @Override
    public void addPatient(Patient p) {
        entitymanager.getTransaction().begin();

        entitymanager.persist(p);
        entitymanager.getTransaction().commit();
    }

    @Override
    public Class getPatientModel() {
        return JpaPatientModel.class;
    }

    @Override
    public Class getTreatmentModel() {
        return JpaTreatmentModel.class;
    }
}
