package elte.dentist.main.model.datasource;

import elte.dentist.main.model.entities.Patient;
import elte.dentist.main.model.entities.Treatment;
import elte.dentist.main.model.entities.mock.MockPatientModel;
import elte.dentist.main.model.entities.mock.MockTreatmentModel;


import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by Mórocz Bálint on 2016. 08. 22..
 */
public class MockDataSource implements DataSource {

    private List<MockPatientModel> mockData;

    public MockDataSource(){
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
        try{
            mockData = new ArrayList<>(
                    Arrays.asList(
                            new MockPatientModel(
                                    "Kiss István",
                                    "Budapest",
                                    new Date(f.parse("1998-05-7").getTime()),
                                    new ArrayList<Treatment>(Arrays.asList(
                                            new MockTreatmentModel("állapotfelmérés"),
                                            new MockTreatmentModel("Fogkő eltávolítás")
                                    ))
                            ),
                            new MockPatientModel(
                                    "Remi Mirtil",
                                    "Budapest",
                                    new Date(f.parse("1987-03-7").getTime()),
                                    new ArrayList<Treatment>(Arrays.asList(
                                            new MockTreatmentModel("Foghúzás"),
                                            new MockTreatmentModel("konzultáció")
                                    ))
                            ),
                            new MockPatientModel(
                                    "Eled Elek",
                                    "Budapest",
                                    new Date(f.parse("1998-05-7").getTime()),
                                    new ArrayList<Treatment>(Arrays.asList(
                                            new MockTreatmentModel("állapotfelmérés"),
                                            new MockTreatmentModel("korona felhelyezés")
                                    ))
                            )
                    )
            );
        }catch(Exception e){
            mockData = new ArrayList<>();
        }


        //Fill the data


    }
    @Override
    public List<Patient> getPatients(){
        return Collections.unmodifiableList(mockData);
    }

    @Override
    public Patient findPatient(long id){
        for(MockPatientModel p : mockData){
            if(p.getId() == id){
                return p;
            }
        }
        return null;
    }

    @Override
    public void removePatient(long id){
        mockData.remove(findPatient(id));
    }

    @Override
    public  void removePatient(Patient p){
        mockData.remove(p);
    }

    @Override
    public void updatePatient(Patient p){
       Patient ref = mockData.get(mockData.indexOf(findPatient(p.getId())));

        ref.setName(p.getName());
        ref.setAddress(p.getAddress());
        ref.setDateOfBirth(p.getDateOfBirth());
        ref.setTreatments(p.getTreatments());
    }

    @Override
    public void addPatient(Patient p){
        mockData.add((MockPatientModel) p);
    }

    @Override
    public Class getPatientModel() {
        return MockPatientModel.class;
    }

    @Override
    public Class getTreatmentModel() {
        return MockTreatmentModel.class;
    }


}
