package elte.dentist.main.model.entities;

import java.sql.Date;
import java.util.List;

/**
 * Created by Mórocz Bálint on 2016. 08. 22..
 */
public abstract class Patient {

    public abstract long getId();
    public abstract void setId(long id);

    public abstract String getName();

    public abstract void setName(String name);

    public abstract String getAddress();

    public abstract void setAddress(String address);

    public abstract Date getDateOfBirth();

    public abstract void setDateOfBirth(Date dateOfBirth);

    public abstract List<Treatment> getTreatments();

    public abstract void setTreatments(List<Treatment> treatments);

    public abstract void addTreatment(Treatment t);

    public abstract void updateTreatment(Treatment t);
}
