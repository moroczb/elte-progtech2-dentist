package elte.dentist.main.model.entities;

import java.sql.Date;

/**
 * Created by Mórocz Bálint on 2016. 08. 22..
 */
public abstract class Treatment {


    public abstract long getId();

    public abstract void setId(long id);

    public abstract String getDescription();

    public abstract void setDescription(String description);

    public abstract Date getDateOfTreatment();

    public abstract void setDateOfTreatment(Date dateOfTreatment);

}
