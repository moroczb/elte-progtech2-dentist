package elte.dentist.main.model.entities.jpa;



import elte.dentist.main.model.entities.Patient;
import elte.dentist.main.model.entities.Treatment;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


/**
 * Created by Mórocz Bálint on 2016. 08. 30..
 */



@Entity(name="Patient")
@NamedQueries(value = {
        @NamedQuery(
                name = "Patient.findAll",
                query = "select p from Patient p"
        )
})
public class JpaPatientModel extends Patient implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String name;
    private String address;
    private Date dateOfBirth;

    @OneToMany(mappedBy = "patient",targetEntity = JpaTreatmentModel.class ,cascade = CascadeType.ALL)
    private List treatments;

    public JpaPatientModel() {
        treatments = new ArrayList<>();
    }

    public JpaPatientModel(String name, String address, Date dateOfBirth) {
        this.name = name;
        this.address = address;
        this.dateOfBirth = dateOfBirth;
        treatments = new ArrayList<>();
    }



    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    @Override
    public List<Treatment> getTreatments() {
        return treatments;
    }

    @Override
    public void setTreatments(List<Treatment> treatments) {

        this.treatments = treatments;
       this.treatments.forEach(x -> ((JpaTreatmentModel) x).setPatient(this));
    }

    @Override
    public void addTreatment(Treatment t) {
        JpaTreatmentModel temp = (JpaTreatmentModel) t;
        temp.setPatient(this);
        treatments.add(temp);
    }

    @Override
    public void updateTreatment(Treatment t) {
        for (Object tk : treatments) {
            JpaTreatmentModel temp = (JpaTreatmentModel) tk;
            if (t.getId() == temp.getId()) {
                temp.setDescription(t.getDescription());
                break;
            }
        }
    }




}
