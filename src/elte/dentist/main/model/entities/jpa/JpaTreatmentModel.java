package elte.dentist.main.model.entities.jpa;

import elte.dentist.main.model.entities.Treatment;

import javax.persistence.*;
import java.sql.Date;

/**
 * Created by Mórocz Bálint on 2016. 08. 30..
 */
@Entity(name="Treatment")
public class JpaTreatmentModel extends Treatment{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String description;
    private Date dateOfTreatment;

    public JpaPatientModel getPatient() {
        return patient;
    }

    public void setPatient(JpaPatientModel patient) {
        this.patient = patient;
    }

    @ManyToOne(optional = false)
    @JoinColumn(name="patient",referencedColumnName = "id")
    private JpaPatientModel patient;

    public JpaTreatmentModel() {
    }

    public JpaTreatmentModel(String description, Date dateOfTreatment) {
        this.description = description;
        this.dateOfTreatment = dateOfTreatment;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateOfTreatment() {
        return dateOfTreatment;
    }

    public void setDateOfTreatment(Date dateOfTreatment) {
        this.dateOfTreatment = dateOfTreatment;
    }
}
