package elte.dentist.main.model.entities.mock;




import elte.dentist.main.model.entities.Patient;
import elte.dentist.main.model.entities.Treatment;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Mórocz Bálint on 2016. 08. 30..
 */
public class MockPatientModel extends Patient {
    private static long counter = 0;

    private long id = counter++;

    private String name;
    private String address;
    private Date dateOfBirth;

    private List<Treatment> treatments = new ArrayList<>();

    public MockPatientModel() {

    }

    public MockPatientModel(String name, String address, Date dateOfBirth) {
        this.name = name;
        this.address = address;
        this.dateOfBirth = dateOfBirth;
    }

    public MockPatientModel(String name, String address, Date dateOfBirth, List<Treatment> treatments) {
        this.name = name;
        this.address = address;
        this.dateOfBirth = dateOfBirth;
        this.treatments = treatments;
    }

    public MockPatientModel(MockPatientModel p){
        this.name = p.name;
        this.address = p.address;
        this.dateOfBirth = p.dateOfBirth;
        this.treatments = p.treatments;
    }

    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    @Override
    public List<Treatment> getTreatments() {
        return treatments;
    }

    @Override
    public void setTreatments(List<Treatment> treatments) {
        this.treatments = treatments;
    }

    @Override
    public void addTreatment(Treatment t) {
        this.treatments.add(t);
    }


    public void updateTreatment(Treatment t){
        for (Treatment temp : treatments) {
            if (t.getId() == temp.getId()) {
                temp.setDescription(t.getDescription());
                break;
            }
        }
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MockPatientModel patient = (MockPatientModel) o;

        if (!name.equals(patient.name)) return false;
        if (!address.equals(patient.address)) return false;
        if (!dateOfBirth.equals(patient.dateOfBirth)) return false;
        return treatments.equals(patient.treatments);

    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + address.hashCode();
        result = 31 * result + dateOfBirth.hashCode();
        result = 31 * result + treatments.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", treatments=" + treatments +
                '}';
    }
}
