package elte.dentist.main.model.entities.mock;

import elte.dentist.main.model.entities.Treatment;

import java.io.Serializable;
import java.sql.Date;

/**
 * Created by Mórocz Bálint on 2016. 08. 30..
 */
public class MockTreatmentModel extends Treatment implements Serializable {
    private static long counter = 0;

    private long id = counter++;

    private String description;
    private Date dateOfTreatment;

    public MockTreatmentModel() {
    }

    public MockTreatmentModel(String description) {
        this.description = description;
        this.dateOfTreatment = new Date(new java.util.Date().getTime());
    }

    public MockTreatmentModel(String description, Date dateOfTreatment) {
        this.description = description;
        this.dateOfTreatment = dateOfTreatment;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateOfTreatment() {
        return dateOfTreatment;
    }

    public void setDateOfTreatment(Date dateOfTreatment) {
        this.dateOfTreatment = dateOfTreatment;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

         MockTreatmentModel treatment = (MockTreatmentModel) o;

        if (description != null ? !description.equals(treatment.description) : treatment.description != null)
            return false;
        return dateOfTreatment != null ? dateOfTreatment.equals(treatment.dateOfTreatment) : treatment.dateOfTreatment == null;

    }

    @Override
    public int hashCode() {
        int result = description != null ? description.hashCode() : 0;
        result = 31 * result + (dateOfTreatment != null ? dateOfTreatment.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Treatment{" +
                "description='" + description + '\'' +
                ", dateOfTreatment=" + dateOfTreatment +
                '}';
    }
}
