package elte.dentist.main.view.button;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Mórocz Bálint on 2016. 08. 31..
 */
public class SubmitButton extends JButton {
    private Object data;
    private SubmitEventListener listener;

    public SubmitButton(SubmitEventListener listener) {
        this.listener = listener;
    }

    private void setData(Object o){
        this.data = o;
    }

    private Object getData(Object o){
        return data;
    }

    public void fireSubmitEvent(Object o){
        this.data = o;
        listener.onSubmitEvent(data);
    }

    public void fireSubmitEvent(){
        listener.onSubmitEvent(data);
    }


}
