package elte.dentist.main.view.button;

/**
 * Created by Mórocz Bálint on 2016. 08. 31..
 */
public interface SubmitEventListener {
    void onSubmitEvent(Object o);
}
