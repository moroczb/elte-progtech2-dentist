package elte.dentist.main.view.dialog;

import elte.dentist.main.model.entities.jpa.JpaPatientModel;
import elte.dentist.main.view.button.SubmitButton;
import elte.dentist.main.view.button.SubmitEventListener;
import org.jdatepicker.JDatePanel;
import org.jdatepicker.JDatePicker;
import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;
import java.util.StringJoiner;

/**
 * Created by Mórocz Bálint on 2016. 08. 30..
 */
public class NewPatientDialog extends JDialog{
    private JTextField name;
    private JTextField address;
    private JDatePanelImpl datePanel;
    private JDatePickerImpl datePicker;
    private SubmitButton submit;
    private JButton cencel;



    public NewPatientDialog(Class classOfData ,SubmitEventListener listener) {
        name = new JTextField();
        address = new JTextField();
        submit = new SubmitButton(listener);
        submit.setText("Páciens felvétele");
        cencel = new JButton("Mégse");

        UtilDateModel model = new UtilDateModel();
        Properties p = new Properties();
        p.put("text.today", "Today");
        p.put("text.month", "Month");
        p.put("text.year", "Year");
        datePanel = new JDatePanelImpl(model, p);
        datePicker = new JDatePickerImpl(datePanel, new JFormattedTextField.AbstractFormatter() {
            private String datePattern = "yyyy-MM-dd";
            private SimpleDateFormat dateFormatter = new SimpleDateFormat(datePattern);

            @Override
            public Object stringToValue(String text) throws ParseException {
                return dateFormatter.parseObject(text);
            }

            @Override
            public String valueToString(Object value) throws ParseException {
                if (value != null) {
                    Calendar cal = (Calendar) value;
                    return dateFormatter.format(cal.getTime());
                }

                return "";
            }
        });
        setLayout(new GridLayout(4,2));
        setSize(new Dimension(400,400));
        add(new JLabel("Név"));
        add(name);

        add(new JLabel("Cím"));
        add(address);

        add(new JLabel("Születési idő"));
        add(datePanel);

        submit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    submit.fireSubmitEvent(
                            classOfData
                                    .getConstructor(String.class, String.class,Date.class)
                                    .newInstance(getNameValue(),getAddressValue(),getDateValue())
                    );
                    dispose();
                } catch (InstantiationException | NoSuchMethodException | IllegalAccessException| InvocationTargetException e1) {
                    e1.printStackTrace();
                }
                catch(NullPointerException ex) {
                    JOptionPane.showMessageDialog(null,"Születési dátumot kötelező megadni","Hiba", JOptionPane.ERROR_MESSAGE);
                }

            }
        });
        add(submit);

        cencel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });

        add(cencel);

        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setModal(true);

        setVisible(true);

    }

    private String getNameValue(){
        return name.getText();
    }

    private String getAddressValue(){
        return address.getText();
    }

    private Date getDateValue(){
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");

        return new Date(((java.util.Date)datePicker.getModel().getValue()).getTime());
    }

}
