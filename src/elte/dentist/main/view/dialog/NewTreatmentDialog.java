package elte.dentist.main.view.dialog;

import elte.dentist.main.view.button.SubmitButton;
import elte.dentist.main.view.button.SubmitEventListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;
import java.sql.Date;

/**
 * Created by Mórocz Bálint on 2016. 08. 30..
 */
public class NewTreatmentDialog extends JDialog {
    private JTextField descriptionField;
    private JLabel dateLabel;
    private Date dateOfTreatment;
    private SubmitButton submit;
    private JButton cencel;

    public NewTreatmentDialog(Class classOfData, SubmitEventListener listener){


        dateOfTreatment = new Date(new java.util.Date().getTime());
        dateLabel = new JLabel(dateOfTreatment.toString());

        descriptionField = new JTextField();
        submit = new SubmitButton(listener);
        submit.setText("Kezelés hozzáadása");
        cencel = new JButton("Vissza");

        setLayout(new GridLayout(3,2));
        setSize(new Dimension(400,400));
        add(new JLabel("Dárum"));
        add(dateLabel);
        add(new JLabel("Leírás"));
        add(descriptionField);


        submit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    submit.fireSubmitEvent(classOfData.getConstructor(String.class,Date.class).newInstance(getDescriptionValue(),dateOfTreatment));
                    dispose();
                } catch (NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e1) {
                    e1.printStackTrace();
                }
            }
        });

        cencel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });


        add(submit);
        add(cencel);

        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setModal(true);
        setVisible(true);



    }

    private String getDescriptionValue(){
        return descriptionField.getText();
    }
}
