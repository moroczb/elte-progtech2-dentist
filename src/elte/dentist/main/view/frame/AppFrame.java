package elte.dentist.main.view.frame;


import elte.dentist.main.controller.AppController;
import elte.dentist.main.model.datasource.JpaDataSource;
import elte.dentist.main.model.datasource.MockDataSource;
import elte.dentist.main.view.panel.DetailPanel;
import elte.dentist.main.view.panel.PatientPanel;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Mórocz Bálint on 2016. 08. 22..
 */
public class AppFrame extends JFrame {
    private AppController appController;

    public AppFrame(){
        setTitle("Fogorvos");

        setLayout(new GridBagLayout());
        setSize(new Dimension(400,600));
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);


        GridBagConstraints constraints = new GridBagConstraints();

        appController = new AppController(
                new JpaDataSource(), //Change this to modify datasource
                new PatientPanel(),
                new DetailPanel()
        );

        constraints.fill = GridBagConstraints.BOTH;
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.weightx =0.3;
        constraints.weighty = 1.0;

        add(appController.getPatientView(),constraints);

        constraints.fill = GridBagConstraints.BOTH;
        constraints.gridx = 1;
        constraints.gridy = 0;
        constraints.weightx = 0.7;
        constraints.weighty = 1.0;

        add(appController.getDetailView(),constraints);
    }


}
