package elte.dentist.main.view.panel;

import elte.dentist.main.controller.DetailPanelStateController;
import elte.dentist.main.model.entities.Patient;
import elte.dentist.main.model.entities.Treatment;
import elte.dentist.main.view.button.SubmitEventListener;
import elte.dentist.main.view.dialog.NewPatientDialog;
import elte.dentist.main.view.dialog.NewTreatmentDialog;
import elte.dentist.main.view.table.treatments.TreatmentTable;
import elte.dentist.main.view.table.treatments.TreatmentTableEventInterface;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Mórocz Bálint on 2016. 08. 22..
 */
public class DetailPanel extends JPanel implements TreatmentTableEventInterface {
    private DetailPanelStateController callback;

    private Patient patient;

    private JLabel nameLabel;
    private JLabel dobLabel;
    private JLabel addressLabel;

    private TreatmentTable treatmentTable;

    private JButton addTreatmentButton;


    public DetailPanel() {
        setLayout(new GridLayout(5,1));
        nameLabel = new JLabel();
        dobLabel = new JLabel();
        addressLabel = new JLabel();
        treatmentTable = new TreatmentTable(this);

        addTreatmentButton = new JButton("Kezelés hozzáadása");
        addTreatmentButton.setEnabled(false);

        addTreatmentButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new NewTreatmentDialog(callback.getTreatmentModel(), new SubmitEventListener() {
                    @Override
                    public void onSubmitEvent(Object o) {
                        Treatment t = (Treatment) o;
                        System.out.println(t);
                        treatmentTable.setTreatments(patient.getTreatments());
                        patient.getTreatments().add(t);
                        System.out.println(patient.getTreatments());
                    }
                });

            }
        });




        add(nameLabel);
        add(dobLabel);
        add(addressLabel);
        add(treatmentTable);
        add(addTreatmentButton);
    }


    public void setPatient(Patient p){
        this.patient = p;
        nameLabel.setText(p.getName());
        dobLabel.setText(p.getDateOfBirth().toString());
        addressLabel.setText(p.getAddress());
        treatmentTable.setTreatments(p.getTreatments());
        addTreatmentButton.setEnabled(true);
    }



    public void setController(DetailPanelStateController controller){
        callback = controller;
    }


    @Override
    public void onTreatmentChange(Treatment t) {
        patient.updateTreatment(t);
        callback.onChangePatient(patient);
    }

    @Override
    public void onTreatmentAdd( Treatment t) {
        patient.addTreatment(t);
       callback.onChangePatient(patient);
    }
}
