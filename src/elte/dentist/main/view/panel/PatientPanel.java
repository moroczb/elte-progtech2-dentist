package elte.dentist.main.view.panel;

import elte.dentist.main.controller.PatientPanelStateController;
import elte.dentist.main.model.entities.Patient;
import elte.dentist.main.view.button.SubmitEventListener;
import elte.dentist.main.view.dialog.NewPatientDialog;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Mórocz Bálint on 2016. 08. 22..
 */
public class PatientPanel extends JPanel {
    private PatientPanelStateController callback;

    private JButton newPatientButton;
    private JButton removePatientButton;

    private DefaultListModel<String> listModel;
    private JList<String> list;


    public PatientPanel() {
        setLayout(new BorderLayout());
        setBackground(Color.red);
        initButtons();
    }

    private void initButtons() {
        JPanel panel = new JPanel(new GridLayout(1,2));
        newPatientButton = new JButton("+");
        newPatientButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new NewPatientDialog(callback.getPatientModel(), new SubmitEventListener() {
                    @Override
                    public void onSubmitEvent(Object o) {
                        Patient p = (Patient) o;
                        listModel.addElement(p.getName());
                        callback.onAddPatient(p);
                    }
                });
            }
        });

        removePatientButton = new JButton("-");
        removePatientButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(list.getSelectedIndex() != -1){
                    callback.onRemovePatient(list.getSelectedIndex());
                    listModel.removeElementAt(list.getSelectedIndex());
                    list.setSelectedIndex(0);
                }

            }
        });
        removePatientButton.setEnabled(false);
        panel.add(newPatientButton);
        panel.add(removePatientButton);
        add(panel,BorderLayout.PAGE_END);
    }

    public void initList(Object[] labels){
         listModel = new DefaultListModel();
        for(Object o : labels){
            listModel.addElement((String)o);
        }
        list = new JList<>(listModel);

        list.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if(!e.getValueIsAdjusting()){
                    if(list.getSelectedIndex() != -1){
                        removePatientButton.setEnabled(true);
                    }
                    callback.onChangeSelectedPatient(list.getSelectedIndex());
                }
            }
        });

        add(list,BorderLayout.CENTER);
    }



    public void setController(PatientPanelStateController controller){
        this.callback = controller;
    }
}
