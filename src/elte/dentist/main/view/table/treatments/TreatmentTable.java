package elte.dentist.main.view.table.treatments;

import elte.dentist.main.model.entities.Treatment;

import javax.swing.*;
import java.util.List;

/**
 * Created by Mórocz Bálint on 2016. 08. 29..
 */
public class TreatmentTable extends JTable {
    private TreatmentTableEventInterface tte;

    public TreatmentTable(TreatmentTableEventInterface tte, List<Treatment> ls) {
        super(new TreatmentTableModel(tte,ls));
        this.tte = tte;
    }


    public TreatmentTable(TreatmentTableEventInterface tte) {
        this.tte = tte;
    }

    public void setTreatments(List<Treatment> ls){
        setModel(new TreatmentTableModel(this.tte,ls));
    }

    public void addTreatment(Treatment t){
        ((TreatmentTableModel)getModel()).addTreatment(t);
    }

}
