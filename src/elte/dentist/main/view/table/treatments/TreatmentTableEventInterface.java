package elte.dentist.main.view.table.treatments;

import elte.dentist.main.model.entities.Treatment;

/**
 * Created by Mórocz Bálint on 2016. 08. 30..
 */
public interface TreatmentTableEventInterface {
    void onTreatmentChange(Treatment t);
    void onTreatmentAdd( Treatment t);
}
