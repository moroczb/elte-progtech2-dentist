package elte.dentist.main.view.table.treatments;

import elte.dentist.main.model.entities.Treatment;

import javax.swing.table.AbstractTableModel;
import java.sql.Date;
import java.util.List;

/**
 * Created by Mórocz Bálint on 2016. 08. 29..
 */
class TreatmentTableModel extends AbstractTableModel {
    private TreatmentTableEventInterface tte;

    private List<Treatment> data;
    private String[] header = {"Dátum", "Leírás"};


    TreatmentTableModel(TreatmentTableEventInterface tte, List<Treatment> data) {
        this.tte = tte;
        this.data = data;
    }

    public TreatmentTableModel(TreatmentTableEventInterface tte) {
        this.tte = tte;
    }

    @Override
    public int getRowCount() {
      return data.size();
    }

    @Override
    public int getColumnCount() {
        return header.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Treatment temp = data.get(rowIndex);

        switch (columnIndex){
            case 0:
                return temp.getDateOfTreatment();
            case 1:
                return temp.getDescription();
            default:
                return null;
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        switch(columnIndex){
            case 0:
                data.get(rowIndex).setDateOfTreatment((Date)aValue);
                tte.onTreatmentChange(data.get(rowIndex));
                break;
            case 1:
                data.get(rowIndex).setDescription((String)aValue);
                tte.onTreatmentChange(data.get(rowIndex));
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return  columnIndex == 1;
    }

    @Override
    public String getColumnName(int column) {
        return header[column];
    }

    public void addTreatment(Treatment t){
        data.add(t);
    }
}
